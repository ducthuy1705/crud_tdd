<?php

namespace Tests\Feature;

use App\Models\Task;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CreateNewTaskTest extends TestCase
{

    public function getCreateRoute()
    {
        return route('task.create');
    }

    public function getStoreRoute()
    {
        return route('task.store');
    }

    /** @test*/
    public function unauthenticated_user_cant_view_create_form()
    {
        $response = $this->get($this->getCreateRoute());
        $response->assertStatus(302);
        $response->assertRedirect('/login');
    }

     /** @test*/
     public function authenticated_user_can_view_create_form()
     {
         $this->actingAs(User::factory()->create());
         $response = $this->get($this->getCreateRoute());
         $response->assertStatus(200);
         $response->assertViewIs('task.form');
     }

     /** @test*/
     public function a_task_requires_a_name()
     {
         $this->actingAs(User::factory()->create());
         $task = Task::factory()->make(['name' => null])->toArray();
         $response = $this->post($this->getStoreRoute(), $task);
         $response->assertSessionHasErrors('name');
     }

      /** @test*/
      public function a_user_can_create_task_no_fail()
      {
          $this->actingAs(User::factory()->create());
          $task = Task::factory()->make()->toArray();
          $response = $this->post($this->getStoreRoute(), $task);
          $response->assertRedirect(route('task.index'));
          $this->assertDatabaseHas('tasks', $task);
      }
}
