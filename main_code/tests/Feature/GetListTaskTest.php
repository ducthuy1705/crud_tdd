<?php

namespace Tests\Feature;

use App\Models\Task;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class GetListTaskTest extends TestCase
{
    /** @test*/
    public function user_can_get_list_task()
    {
        $response = $this->get('/task');
        $response->assertStatus(200);
        $response->assertViewIs('task.index');
    }

    /** @test*/
    public function user_can_see_single_record()
    {
        $task = Task::factory()->create();
        $response = $this->get('/task/show/'.$task->id);
        $response->assertStatus(302);
    }
}
