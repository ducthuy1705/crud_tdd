<?php

namespace Tests\Feature;

use App\Models\Task;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class DeleteTaskTest extends TestCase
{
    public function getDeleteRoute($id)
    {
        return route('task.destroy', $id);
    }

    /** @test*/
    public function unauthenticated_user_cant_delete_task()
    {
        $task = Task::factory()->create();
        $response = $this->delete($this->getDeleteRoute($task->id));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    /** @test*/
    public function authenticated_user_can_delete_task()
    {
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->create();
        $response = $this->delete($this->getDeleteRoute($task->id));
        $response->assertStatus(Response::HTTP_FOUND);
    }
}
