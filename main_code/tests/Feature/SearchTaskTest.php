<?php

namespace Tests\Feature;

use App\Models\Task;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class SearchTaskTest extends TestCase
{
    /** @test*/
    public function user_can_search_task_by_name()
    {
        $task = Task::factory()->create();
        $response = $this->get('/task/search');
        $response->assertViewIs('task.search');
        $response->assertSeeText($task->name);
        $response->assertStatus(200);
    }
}
