<?php

namespace Tests\Feature;

use App\Models\Task;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;

class UpdateTaskTest extends TestCase
{

    public function getEditRoute($id)
    {
        return route('task.edit',$id);
    }

    public function getUpdateRoute($id)
    {
        return route('task.update',$id);
    }

    /** @test*/
    public function unauthenticated_user_cant_view_edit_form()
    {
        $task = Task::factory()->create();
        $response = $this->get($this->getEditRoute($task->id));
        $response->assertStatus(302);
        $response->assertRedirect('/login');
    }

     /** @test*/
     public function authenticated_user_can_view_edit_form()
     {
         $this->actingAs(User::factory()->create());
         $response = $this->get($this->getEditRoute($task->id));
         $response->assertStatus(200);
         $response->assertViewIs('task.form');
     }

     /** @test*/
     public function a_task_update_requires_a_name()
     {
         $this->actingAs(User::factory()->create());
         $task = Task::factory()->create(['name' => null]);
         $response = $this->put($this->getUpdateRoute($task->id), $task->toArray());
         $response->assertStatus(302);
         $response->assertSessionHasErrors('name');
     }

     /** @test*/
     public function authenticated_user_can_edit_task()
     {
         $this->actingAs(User::factory()->create());
         $task = Task::factory()->create();
         $task->name = "Hello";
         $this->put($this->getUpdateRoute($task->id),$task->toArray());
         $this->assertDatabaseHas('tasks', ['id' => $task->id, 'name' => "Hello"]);
     }
}
