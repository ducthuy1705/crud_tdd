@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">{{ $task->name }}</h5>
                        <p class="card-text">{{ $task->description }}</p>
                        <p class="card-text">{{ date_format($task->created_at, 'd/m/Y h:i:s') }}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
