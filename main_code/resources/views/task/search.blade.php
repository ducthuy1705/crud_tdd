@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="d-flex justify-content-between">
                    <form action="{{ route("task.search") }}" method="GET">
                        <div class="form-group d-flex">
                            <input type="text" name="key" id="" class="form-control" placeholder="Search by name...">
                            <button type="submit" class="btn btn-success ml-2">Submit</button>
                        </div>
                    </form>
                
                </div>
                <table class="table table-dark">
                    <thead>
                        <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Name</th>
                            <th scope="col">Descirption</th>
                            <th scope="col">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($tasks as $item)
                            <tr>
                                <th>{{ $item->id }}</th>
                                <th>{{ $item->name }}</th>
                                <th>{{ $item->description }}</th>
                                <th>
                                    <form action="{{ route('task.destroy', ['id' => $item->id]) }}" method="POST">
                                        @csrf
                                        @method('delete')
                                        <div class="d-flex">
                                            <a href="{{ route('task.edit', ['id' => $item->id]) }}">Sửa</a>
                                            <button type="submit" class="btn">Xóa</button>
                                            <a href="{{ route('task.show', ['id' => $item->id]) }}">Chi tiết</a>
                                        </div>  
                                    </form>
                                </th>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
    
            </div>
        </div>
    </div>
@endsection
