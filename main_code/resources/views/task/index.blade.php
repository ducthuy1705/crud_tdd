@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="d-flex justify-content-between">
                    <a href="{{ route('task.create') }}">
                        <button class="btn btn-success mb-3">Create New Task</button>
                    </a>
                    <form action="{{ route("task.search") }}" method="GET">
                        <div class="form-group d-flex">
                            <input type="text" name="key" id="" class="form-control" placeholder="Search by name...">
                            <button type="submit" class="btn btn-success ml-2">Submit</button>
                        </div>
                    </form>
                
                </div>

                @include('components.alert')
                <table class="table table-dark">
                    <thead>
                        <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Name</th>
                            <th scope="col">Descirption</th>
                            <th scope="col" style="width: 150px">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($tasks as $item)
                            <tr>
                                <th>{{ $item->id }}</th>
                                <th>{{ $item->name }}</th>
                                <th>{{ $item->description }}</th>
                                <th>
                                    <form action="{{ route('task.destroy', ['id' => $item->id]) }}" method="POST">
                                        @csrf
                                        @method('delete')
                                        <div class="d-flex">
                                            <a href="{{ route('task.edit', ['id' => $item->id]) }}">
                                                <i class="fas fa-pencil-alt"></i>
                                            </a>
                                            <button type="submit" class="btn-custom">
                                                <i class="fas fa-trash"></i>
                                            </button>
                                            <a href="{{ route('task.show', ['id' => $item->id]) }}">
                                                <i class="fas fa-search"></i>
                                            </a>
                                        </div>  
                                    </form>
                                </th>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {{ $tasks->links() }}
            </div>
        </div>
    </div>
@endsection
