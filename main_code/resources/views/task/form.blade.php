@php
    if(!empty($singleRecord)){
        $action = route('task.update', ['id' => $singleRecord->id]);
    }
    else{
        $action = route('task.store');
    }
@endphp
@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <form action="{{ $action }}" method="POST">
                    @if (!empty($singleRecord))
                        @method('PUT')
                    @endif

                    @csrf
                    <div class="form-group">
                        <label for="">Task Name</label>
                        @if ($errors -> has('name'))
                            <p class="text-danger">{{ $errors -> first('name') }}</p>
                        @endif
                        <input type="text" name="name" class="form-control" value="{{ old('name', @$singleRecord->name) }}">
                    </div>
                    <div class="form-group">
                        <label for="">Task Description</label>
                        <input type="text" name="description" class="form-control" value="{{ old('name', @$singleRecord->description) }}">
                    </div>
                    <button type="submit" class="btn btn-success">Save</button>
                </form>
            </div>
        </div>
    </div>
@endsection