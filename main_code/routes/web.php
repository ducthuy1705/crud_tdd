<?php

use App\Http\Controllers\TaskController;
use App\Models\Task;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function(){
    return view('welcome');
});

Route::prefix('task')->name('task.')->group( function(){
    $taskClaas = TaskController::class;
    Route::get('/', [$taskClaas, 'index'])->name('index');

    Route::middleware('auth')->group(function() use($taskClaas){
        Route::get('create', [$taskClaas, 'create'])->name('create');
        Route::post('store', [$taskClaas, 'store'])->name('store');
        Route::get('edit/{id}', [$taskClaas, 'edit'])->name('edit');
        Route::put('update/{id}', [$taskClaas, 'update'])->name('update');
        Route::delete('delete/{id}', [$taskClaas, 'destroy'])->name('destroy');
    });

    Route::get('show/{id}', [$taskClaas, 'show'])->name('show');
    Route::get('search', [$taskClaas, 'search'])->name('search');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
