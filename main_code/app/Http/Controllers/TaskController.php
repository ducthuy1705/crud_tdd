<?php

namespace App\Http\Controllers;

use App\Models\Task;
use Illuminate\Http\Request;
use App\Http\Requests\TaskRequest;
use Illuminate\Support\Facades\Session;

class TaskController extends Controller
{
    protected $pathView = 'task.';
    protected $model;

    public function __construct(Task $model)
    {
        $this->model = $model;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tasks = $this->model->paginate(10);
        return view($this->pathView.'index', compact('tasks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->pathView.'form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TaskRequest $request)
    {
        $this->model->create($request->all());
        Session::flash("created", "Thêm mới thành công");
        return redirect()->route('task.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $task = $this->model->findOrFail($id);
        return view($this->pathView."show", compact('task'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $singleRecord = $this->model->findOrFail($id);
        return view($this->pathView.'form', compact('singleRecord'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function update(TaskRequest $request, $id)
    {
        $task = $this->model->findOrFail($id);
        $task->update($request->all());
        Session::flash("updated", "Sửa thành công task có id = ".$id);
        return redirect()->route('task.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $task = $this->model->findOrFail($id);
        $task->delete();
        Session::flash("deleted", "Xóa thành công task có id = ".$id);
        return redirect()->back();        
    }

    public function search(Request $request)
    {
        $tasks = $this->model->where('name', 'like', '%'.$request->key .'%')->get();
        return view($this->pathView.'search',compact('tasks'));
    }
}
